/*
 * assert() permuter() decallerADroite()
 */
package testclasslabo5;

import java.util.Arrays;

/**
 *
 * @author TUNSAJAN Somboom (G3)
 */
public class TestClassLabo5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.print("Test decallageTableau()...");
        assert(testDecallageTablau(new String[]{"a"}, new String[]{"a"})) == true; 
        assert(testDecallageTablau(new String[]{"a", "b"}, new String[]{"b", "a"})) == true; 
        assert(testDecallageTablau(new String[]{"a", "b", "c"}, new String[]{"c", "a", "b"})) == true; 
        System.out.println(" OK");
        
        System.out.print("Test permuter()...");
        assert(testPermuter(new String[]{"a"}, new String[]{"a"}, 0,0)) == true;
        assert(testPermuter(new String[]{"a", "b"}, new String[]{"b", "a"}, 0,1)) == true;
        assert(testPermuter(new String[]{"a", "b", "c"}, new String[]{"b", "a", "c"}, 0,1)) == true;
        assert(testPermuter(new String[]{"a", "b", "c"}, new String[]{"c", "b", "a"}, 2,0)) == true;
        System.out.println(" OK");
        // TODO code application logic here
    }
    
    public static boolean testDecallageTablau(String[] tab1, String[] tab2){
        decallageTableau(tab1);
        return Arrays.equals(tab1, tab2);
    }
    public static boolean testPermuter(String[] tab1, String tab2[], int indiceGauche, int indiceDroite){
        permuter(tab1, indiceGauche, indiceDroite);
        return Arrays.equals(tab1, tab2);
    }
    public static boolean decallageTableau(String[] tableauADecaler) {
        if (tableauADecaler == null) {
            return false; /* pas de données dans le tableau */

        }
        String dernierElement = tableauADecaler[tableauADecaler.length - 1]; /* On sauvegarde le dernier element */

        for (int i = tableauADecaler.length - 1; i > 0; i--) {
            tableauADecaler[i] = tableauADecaler[i - 1]; /* On decale vers la droite */

        }
        tableauADecaler[0] = dernierElement;
        return true;
    }
    /* Permet de permuter deux element d un tableau à une dimension
     * (On considere que les element sont present dans le tableau)
     * @param String[] arrayString dont on doit permuter 2 elements
     *        int indiceAPermuterG premier element
     *        int indiceAPermuterD deuxieme element a permuter
     * @return boolean si tout c est bien passé
     */

    public static boolean permuter(String[] arrayString, int indiceAPermuterG, int indiceAPermuterD) {
        /* Parametres valides ? */
        if (arrayString == null
                || arrayString[indiceAPermuterG] == null
                || arrayString[indiceAPermuterD] == null
                || indiceAPermuterG == indiceAPermuterD) {
            return false;
        }
        String tmp = arrayString[indiceAPermuterG]; /* on sauvegarde l element de gauche */

        arrayString[indiceAPermuterG] = arrayString[indiceAPermuterD];
        arrayString[indiceAPermuterD] = tmp;
        return true;
    }
    /* Permet de permuter deux element d un tableau à une dimension
     * (On considere que les element sont present dans le tableau)
     * @param String[] tableau dont on doit permuter 2 elements
     *        int indiceAPermuterG premier element
     *        int indiceAPermuterD deuxieme element a permuter
     * @return void
     */

    public static boolean permuter(int[][] arrayInt, int indiceG, int indiceD) {
        if (arrayInt == null
                || arrayInt[indiceG] == null
                || arrayInt[indiceD] == null
                || indiceG == indiceD) {
            return false;
        }
        int[] tmp = arrayInt[indiceG];
        arrayInt[indiceG] = arrayInt[indiceD];
        arrayInt[indiceD] = tmp;
        return true;
    }

}
